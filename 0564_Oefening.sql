USE ModernWays;

CREATE TABLE IF NOT EXISTS Studenten(
StudentenNummer INT AUTO_INCREMENT PRIMARY KEY,
Voornaam	VARCHAR(100)CHAR SET utf8mb4 NOT NULL, 
Achternaam VARCHAR(100)CHAR SET utf8mb4 NOT NULL
);
CREATE TABLE IF NOT EXISTS Opleidingen(
Naam VARCHAR(100) NOT NULL PRIMARY KEY
);
CREATE TABLE IF NOT EXISTS Vakken(
Naam VARCHAR(100) NOT NULL PRIMARY KEY
);
CREATE TABLE IF NOT EXISTS Lectoren(
Personeelsnummer INT AUTO_INCREMENT PRIMARY KEY,
Voornaam VARCHAR(100)CHAR SET utf8mb4 NOT NULL,
Achternaam VARCHAR(100)CHAR SET utf8mb4 NOT NULL
);
ALTER TABLE Studenten
ADD COLUMN Semester TINYINT UNSIGNED NOT NULL,
ADD COLUMN Opleidingen_Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
ADD CONSTRAINT fk_Studenten_Opleiding
FOREIGN KEY (Opleidingen_Naam)
REFERENCES Opleidingen(Naam);

CREATE TABLE IF NOT EXISTS LectorGeeftVak(
Lectoren_Personeelsnummer INT NOT NULL,
Vakken_Naam VARCHAR(100) CHAR SET utf8mb4,
CONSTRAINT fk_LectorenGeeftVak_Lectoren
FOREIGN KEY (Lectoren_Personeelsnummer)
REFERENCES Lectoren(Personeelsnummer),
CONSTRAINT fk_LectorenGeeftVak_Vakken
FOREIGN KEY (Vakken_Naam)
REFERENCES Vakken(Naam)
);

CREATE TABLE IF NOT EXISTS VakOpleidingsonderdeelOpleiding (
Opleidingen_Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Vakken_Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Semester TINYINT UNSIGNED NOT NULL,
CONSTRAINT fk_VakOpleidingsonderdeelOpleiding_Opleidingen
FOREIGN KEY (Opleidingen_Naam)
REFERENCES Opleidingen(Naam),

CONSTRAINT fk_VakOpleidingsonderdeelOpleiding_Vakken
FOREIGN KEY (Vakken_Naam)
REFERENCES Vakken(Naam)
);






