USE ModernWays;
CREATE VIEW GemiddeldeRatings
AS
SELECT Reviews.Boeken_Id, AVG(Reviews.Rating) AS "Rating" FROM Reviews GROUP BY Reviews.Boeken_Id;