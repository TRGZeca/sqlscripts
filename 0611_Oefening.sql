USE ModernWays;
SELECT Games.Titel, "Geen platformen gekend" FROM Games
LEFT JOIN Releases ON Games.Id = Releases.Games_Id
WHERE Releases.Games_Id IS NULL
UNION ALL
SELECT "Geen games gekend", Platformen.Naam FROM Platformen
LEFT JOIN Releases ON Platformen.ID =Releases.Platformen_Id 
WHERE Releases.Platformen_Id IS NULL;