USE TussentijdseEvaluatie;
SET SQL_SAFE_UPDATES = 0;
DELETE FROM Planten WHERE GrondType COLLATE utf8mb4_0900_ai_ci = "Zure Grond";
SET SQL_SAFE_UPDATES = 1;