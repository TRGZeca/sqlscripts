USE TussentijdseEvaluatie;
CREATE TABLE IF NOT EXISTS Personen(
Voornaam VARCHAR(100) CHARSET utf8mb4 NOT NULL,
Familienaam VARCHAR(100) CHARSET utf8mb4 NOT NULL,
GeboorteDatum DATE NOT NULL,
GsmNummer CHAR(10) NOT NULL
);
