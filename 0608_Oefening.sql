USE ModernWays;
SELECT Leden.Voornaam, Taken.Omschrijving
FROM Leden
RIGHT JOIN Taken
ON Leden.Id = Taken.Leden_Id;