USE TussentijdseEvaluatie1BDB;
SELECT Boeken.Titel FROM Boeken
RIGHT JOIN Ontleningen ON Boeken.Id = Boeken_Id
GROUP BY Boeken.Titel 
HAVING COUNT(Boeken_ID) >=3;