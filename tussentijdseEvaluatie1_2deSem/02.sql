USE TussentijdseEvaluatie1BDB;
CREATE VIEW OnpopulaireBoeken
AS
SELECT Boeken.Titel FROM Boeken
LEFT JOIN Ontleningen ON Boeken_Id = Boeken.Id
WHERE Boeken_Id IS Null;