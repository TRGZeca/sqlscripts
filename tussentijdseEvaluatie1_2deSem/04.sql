USE TussentijdseEvaluatie1BDB;
CREATE VIEW BoekenInfo AS
SELECT Boeken.Titel, Auteurs.Familienaam as "Naam auteur",  Uitgeverijen.Naam as "Naam uitgeverij"FROM Boeken
INNER JOIN Auteurs ON Auteurs.Id = Auteurs_Id
INNER JOIN Uitgeverijen ON Uitgeverijen.Id = Uitgeverijen_Id;
