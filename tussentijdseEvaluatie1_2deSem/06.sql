USE TussentijdseEvaluatie1BDB;
SELECT CONCAT(Leden.Voornaam," ",Leden.Familienaam) as "Volledige Naam"
FROM Leden
LEFT JOIN Ontleningen ON Leden.Id = Leden_Id
WHERE Leden_Id is null
UNION ALL

SELECT Boeken.Titel
FROM Ontleningen
RIGHT JOIN Boeken ON Boeken_Id = Boeken.Id

WHERE Boeken_Id is null;




