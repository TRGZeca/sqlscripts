USE TussentijdseEvaluatie2;
SELECT Regisseur,Genre,AVG(Sterren) AS 'Gemiddeld aantal sterren' 
FROM Films 
WHERE Jaar >=2000 
GROUP BY Regisseur,Genre
HAVING AVG(Sterren) >=4;