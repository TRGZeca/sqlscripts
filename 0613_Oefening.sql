USE ModernWays;
CREATE VIEW AuteursBoeken
AS
SELECT Concat(Personen.Voornaam,' ',Personen.Familienaam) AS "Auteur", Boeken.Titel  
FROM Personen 
INNER JOIN Publicaties ON Publicaties.Personen_Id = Personen.Id 
INNER JOIN Boeken On Publicaties.Boeken_Id = Boeken.Id;