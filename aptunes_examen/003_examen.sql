USE aptunes_examen;
DROP PROCEDURE IF EXISTS PopulateLiedjesGenres;
DELIMITER $$
CREATE PROCEDURE PopulateLiedjesGenres()
BEGIN
DECLARE numberOfLiedjes INT DEFAULT 0;
DECLARE numberOfGenres INT DEFAULT 0;
DECLARE randomLiedjeId INT DEFAULT 0;
DECLARE randomGenreId INT DEFAULT 0;
SELECT COUNT(*) INTO numberOfLiedjes FROM Liedjes;
SELECT COUNT(*) INTO numberOfGenres FROM Genres;
SET randomLiedjeId = floor(rand() * numberOfLiedjes)+1;
SET randomGenreId = floor(rand()*numberOfGenres)+1;

IF (randomLiedjeId, randomGenreId) NOT IN (SELECT * FROM Liedjesgenres)THEN
INSERT INTO LiedjesGenres(Genres_Id,Liedjes_Id)
VALUES(randomGenreId,randomLiedjeId);
END IF;

	
END$$
DELIMITER ;