USE aptunes_examen;
DROP PROCEDURE IF EXISTS GetSongDuration;
DELIMITER $$

CREATE PROCEDURE GetSongDuration(IN bandId VARCHAR(50))
BEGIN
	DECLARE result VARCHAR(50);
	DECLARE songDuration TINYINT UNSIGNED DEFAULT 0;
    DECLARE ok BOOL DEFAULT FALSE;
    DECLARE totalDuration SMALLINT UNSIGNED;
	DECLARE songDurationCursor CURSOR FOR SELECT Lengte FROM Liedjes WHERE Bands_Id = BandId;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET ok = TRUE;
    SET totalDuration = 0;
	
    
    OPEN songDurationCursor;
    fetchloop : LOOP
		FETCH songDurationCursor INTO songDuration;
        IF ok = TRUE THEN 
			LEAVE fetchloop; 
		END IF;
        SET totalDuration = totalDuration+ songDuration;
	END LOOP;
   
    CLOSE songDurationCursor;
     IF totalDuration > 60 THEN
		SET result =  'Lange duurtijd';
	ELSE 
		SET result = 'Normale duurtijd';
	END IF;
   
   SELECT result AS 'Duurtijd';
END$$
delimiter ;