USE aptunes;
DROP PROCEDURE IF EXISTS CreateAndReleaseAlbum;
DELIMITER $$
CREATE PROCEDURE CreateAndReleaseAlbum (IN titelAlbum VARCHAR(100), IN bands_Id INT)
BEGIN
START TRANSACTION;
INSERT INTO Albums (Titel)
VALUES (titelAlbum);
INSERT INTO Albumreleases(Bands_Id,Albums_Id)
VALUES (bands_Id,last_insert_id());
COMMIT;
END$$
DELIMITER ;