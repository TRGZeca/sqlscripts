USE aptunes;
DELIMITER $$
CREATE PROCEDURE NumberOfGenres(OUT aantalGenres TINYINT)
BEGIN
	SELECT DISTINCT COUNT(*)
    INTO aantalGenres
    FROM Genres;
    
END $$
DELIMITER ;