USE ModernWays;
ALTER VIEW AuteursBoeken
AS
SELECT CONCAT(Personen.Voornaam,' ',Personen.Familienaam) AS "Auteur", Boeken.Titel,Boeken.Id AS "Boeken_Id" FROM Boeken
INNER JOIN Publicaties ON Publicaties.Boeken_Id = Boeken.Id
INNER JOIN Personen ON Publicaties.Personen_Id = Personen.Id;