USE aptunes;
DROP PROCEDURE IF EXISTS CleanupOldMemberships;
DELIMITER $$
CREATE PROCEDURE CleanupOldMemberships (IN Datum DATE, OUT Aantal INT)
BEGIN
START TRANSACTION;
SET SQL_SAFE_UPDATES = 0;
DELETE
FROM Lidmaatschappen
Where Lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < Datum;
SET SQL_SAFE_UPDATES = 1;
COMMIT;
END$$
DELIMITER ;