USE ModernWays;
CREATE TABLE Uitleningen
(
Boeken_Id INT NOT NULL,
Leden_Id INT NOT NULL,
Startdatum DATE NOT NULL,
Einddatum DATE,
CONSTRAINT fk_Uitleningen_Boeken FOREIGN KEY (Boeken_Id) REFERENCES Boeken(Id),
CONSTRAINT fk_Uitleningen_Leden FOREIGN KEY (Leden_Id) REFERENCES Leden(Id)
);